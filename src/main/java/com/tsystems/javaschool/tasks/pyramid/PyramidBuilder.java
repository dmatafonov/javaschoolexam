package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    private static final int PYRAMID_SIZE_MAX = 50;

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null) || inputNumbers.size() > PYRAMID_SIZE_MAX) {
            throw new CannotBuildPyramidException();
        }
        try {
            Collections.sort(inputNumbers);
        } catch (Exception r) {
            throw new CannotBuildPyramidException();
        }
        List<List<Integer>> row = new ArrayList<>();
        int size = inputNumbers.size();
        int idx = 0;
        int numRows = 0;
        while (idx < size) {
            numRows++;
            List<Integer> column = new ArrayList<>();
            for (int numInRow = 0; numInRow < numRows; numInRow++) {
                if (idx >= size)
                    throw new CannotBuildPyramidException();
                column.add(inputNumbers.get(idx));
                if (numInRow < numRows - 1)
                    column.add(0);
                idx++;
            }
            row.add(column);
        }
        int columnSize = (numRows * 2) - 1;
        int[][] result = new int[row.size()][columnSize];
        for (int i = 0; i < result.length; i++) {
            List<Integer> column = row.get(i);
            boolean left = true;
            while (column.size() < columnSize) {
                if (left) {
                    column.add(0, 0);
                    left = false;
                } else {
                    column.add(0);
                    left = true;
                }
            }
            for (int j = 0; j < column.size(); j++) {
                result[i][j] = column.get(j);
            }
        }
        return result;
    }

}
