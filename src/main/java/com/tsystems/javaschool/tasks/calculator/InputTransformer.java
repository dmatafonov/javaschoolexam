package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class InputTransformer {

    private Stack<Character> characterStack;
    private String input;
    private String output = "";

    public InputTransformer(String in) {
        input = in;
        characterStack = new Stack<>();
    }

    public String doTranslationToPostfix() {
        for (int j = 0; j < input.length(); j++) {
            char ch = input.charAt(j);
            if (ch == ' ')
                continue;
            if (ch == '+' || ch == '-') {
                output += ' ';
                getOperand(ch, 1);
                continue;
            }
            if (ch == '*' || ch == '/') {
                output += ' ';
                getOperand(ch, 2);
                continue;
            }
            if (ch == '(') {
                characterStack.push(ch);
                continue;
            }
            if (ch == ')') {
                int res = gotParentheses();
                if (res == 0) return "";
            } else {
                output += ch;
            }
        }
        while (!characterStack.isEmpty()) {
            output += ' ';
            output += characterStack.pop();
        }
        return output;
    }


    public void getOperand(char opThis, int prec1) {
        while (!characterStack.isEmpty()) {
            char opTop = characterStack.pop();
            if (opTop == '(') {
                characterStack.push(opTop);
                break;
            } else {
                int prec2;
                if (opTop == '+' || opTop == '-')
                    prec2 = 1;
                else
                    prec2 = 2;
                if (prec2 < prec1) {
                    characterStack.push(opTop);
                    break;
                } else {
                    output = output + opTop + ' ';
                }
            }
        }
        characterStack.push(opThis);
    }

    public int gotParentheses() {
        if (characterStack.size() < 2)
            return 0;
        while (!characterStack.isEmpty()) {
            char chx = characterStack.pop();
            if (chx == '(') {
                break;
            } else {
                output += ' ';
                output += chx;
            }
        }
        return 1;
    }
}
