package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class Calculator {

    private Stack<Double> stack;
    private String postfix;

    public Calculator() {
        stack = new Stack<>();
        postfix = "";
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.isEmpty()) {
            return null;
        } else {
            InputTransformer transformer = new InputTransformer(statement);
            postfix = transformer.doTranslationToPostfix();
            double val;
            double tmpResult = 0;
            double num1, num2;
            if (postfix.isEmpty()) return null;
            String[] tmp = postfix.split(" ");
            for (int j = 0; j < tmp.length; j++) {
                if (tmp[j].isEmpty())
                    return null;
                if (!tmp[j].equals("+") && !tmp[j].equals("-") &&
                        !tmp[j].equals("*") && !tmp[j].equals("/")) {
                    try {
                        val = Double.valueOf(tmp[j]);
                    } catch (NumberFormatException ex) {
                        return null;
                    }
                    stack.push(val);
                } else {
                    num2 = Double.valueOf(stack.pop());
                    num1 = Double.valueOf(stack.pop());
                    switch (tmp[j]) {
                        case "+":
                            tmpResult = num1 + num2;
                            break;
                        case "-":
                            tmpResult = num1 - num2;
                            break;
                        case "*":
                            tmpResult = num1 * num2;
                            break;
                        case "/":
                            if (num2 == 0)
                                return null;
                            tmpResult = num1 / num2;
                            break;
                    }
                    stack.push(tmpResult);
                }
            }
        }

        return format(stack.pop());
    }

    private String format(double d)
    {
        return (d == (long) d) ? String.format("%d", (long)d) : String.format("%s", d);
    }
}
