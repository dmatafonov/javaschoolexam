package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) throws IllegalArgumentException {
        if (x == null || y == null)
            throw new IllegalArgumentException(String.format("x is %b and y is %b", x, y));
        else {
            boolean result=true;
            int k=0;
            for (Object o : x) {
                if (!result) {
                    break;
                }
                result = false;
                for (int j = k; j < y.size(); j++) {
                    if (y.get(j).equals(o)) {
                        k = j + 1;
                        result = true;
                        break;

                    }
                }
            }
            return result;
        }
    }
}
